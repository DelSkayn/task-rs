//! A drop in replacement for RwLock
//! Instead of stalling the thread this implementation tries do work.

use std::sync::RwLock as StdRwLock;
pub use std::sync::{TryLockError, TryLockResult, LockResult, RwLockReadGuard, RwLockWriteGuard};


use rt::worker;

#[derive(Debug,Default)]
pub struct RwLock<T: ?Sized>(StdRwLock<T>);

impl<T> RwLock<T> {
    #[inline]
    pub fn new(t: T) -> Self {
        RwLock(StdRwLock::new(t))
    }
}

impl<T: ?Sized> RwLock<T> {
    #[inline]
    pub fn try_read(&self) -> TryLockResult<RwLockReadGuard<T>> {
        self.0.try_read()
    }

    #[inline]
    pub fn try_write(&self) -> TryLockResult<RwLockWriteGuard<T>> {
        self.0.try_write()
    }

    #[inline]
    pub fn get_mut(&mut self) -> LockResult<&mut T> {
        self.0.get_mut()
    }

    #[inline]
    pub fn is_poisoned(&self) -> bool {
        self.0.is_poisoned()
    }

    #[inline]
    pub fn into_inner(self) -> LockResult<T>
        where T: Sized
    {
        self.0.into_inner()
    }

    pub fn read(&self) -> LockResult<RwLockReadGuard<T>> {
        loop {
            match self.0.try_read() {
                Ok(x) => return Ok(x),
                Err(e) => {
                    match e {
                        TryLockError::Poisoned(x) => return Err(x),
                        TryLockError::WouldBlock => {}
                    }
                }
            }
            if !worker::work() {
                return self.0.read()
            }
        }
    }

    pub fn write(&self) -> LockResult<RwLockWriteGuard<T>> {
        loop {
            match self.0.try_write() {
                Ok(x) => return Ok(x),
                Err(e) => {
                    match e {
                        TryLockError::Poisoned(x) => return Err(x),
                        TryLockError::WouldBlock => {}
                    }
                }
            }
            if !worker::work() {
                return self.0.write()
            }
        }
    }
}
