//! A sync structure for reading data while an other thread can write to it.
//! Simular to a seglock(?,is that the name). however were a seglock needs to lock mutex for write
//! access and can have multiple writers. This implementation can only have one and is completely
//! lock free.

use std::sync::atomic::{AtomicUsize, Ordering};
use std::sync::Arc;
use std::cell::UnsafeCell;

use std::ops::{Deref, DerefMut};

use rt::worker;

use std::mem;

// fuck
//
// needs to be Copy,
//
// Take for instance a arc which is being copied
// The pointer is loaded for cloning then an other thread
// throws away arc it deletes it self. the pointer which the
// origonal thread has is now invalid.
//
// It is possible that if we restrict types which are not
// copy to be non mutable then we can still use this.
// Unfortunatly we cant create a trait which will aply to
// a struct if the Child types implement the trait.
// So we need to restrict this to Copy

pub struct MutatorGuard<'a, T: Copy + 'a> {
    borrow: &'a mut T,
    version: &'a AtomicUsize,
}

struct Data<T: Copy> {
    data: UnsafeCell<T>,
    version: AtomicUsize,
}

unsafe impl<T: Copy> Sync for Data<T> {}
unsafe impl<T: Copy> Send for Data<T> {}

/// The struct which allows to mutate the data.
pub struct Mutator<T: Copy>(Arc<Data<T>>);

/// The part which allows reading of the data.
pub struct Inspector<T: Copy> {
    d: Arc<Data<T>>,
    version: AtomicUsize,
}

impl<T: Copy> Clone for Inspector<T> {
    fn clone(&self) -> Self {
        Inspector {
            d: self.d.clone(),
            version: AtomicUsize::new(self.version.load(Ordering::Acquire)),
        }
    }
}

/// Create the mutator and inpector from given data.
pub fn mutate_inspect<T: Copy>(data: T) -> (Mutator<T>, Inspector<T>) {
    let d = Arc::new(Data {
        data: UnsafeCell::new(data),
        version: AtomicUsize::new(0),
    });
    (Mutator(d.clone()),
     Inspector {
        d: d.clone(),
        version: AtomicUsize::new(0),
    })
}

impl<T: Copy> Mutator<T> {
    pub fn borrow(&self) -> &T {
        unsafe { mem::transmute(self.0.data.get()) }
    }

    pub fn borrow_mut<'a>(&'a mut self) -> MutatorGuard<'a, T> {
        self.0.version.fetch_add(1, Ordering::AcqRel);
        unsafe {
            MutatorGuard {
                borrow: mem::transmute(self.0.data.get()),
                version: &self.0.version,
            }
        }
    }

    pub fn inspector_present(&self) -> bool{
        Arc::strong_count(&self.0) != 1
    }
}

impl<T: Copy> Inspector<T> {
    pub fn mutator_present(&self) -> bool {
        Arc::strong_count(&self.d) != 1
    }

    pub fn changed(&self) -> bool {
        self.version.load(Ordering::Acquire) != self.d.version.load(Ordering::Acquire)
    }

    pub fn get(&self) -> T {
        let mut res;
        loop {
            self.version.store(self.d.version.load(Ordering::Acquire), Ordering::Release);
            if (self.version.load(Ordering::Acquire) | 1) == 0 {
                println!("Currently changed!");
                // currently being changed.
                worker::work();// not sure if this should be here.
                continue;
            }

            unsafe {
                res = (*self.d.data.get()).clone();
            }

            if !self.changed() {
                break;
            }
        }
        res
    }
}

impl<'a, T: Copy> Deref for MutatorGuard<'a, T> {
    type Target = T;
    fn deref(&self) -> &T {
        self.borrow
    }
}

impl<'a, T: Copy> DerefMut for MutatorGuard<'a, T> {
    fn deref_mut(&mut self) -> &mut T {
        self.borrow
    }
}

impl<'a, T: Copy> Drop for MutatorGuard<'a, T> {
    fn drop(&mut self) {
        self.version.fetch_add(1, Ordering::Acquire);
    }
}
