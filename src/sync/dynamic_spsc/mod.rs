mod unsafe_spsc;

use ::crossbeam::epoch::{self, Guard};

use self::unsafe_spsc::UnsafeSpsc;
use self::unsafe_spsc::Ref as UnsafeRef;

use std::sync::Arc;
use std::any::Any;
use std::cell::UnsafeCell;

struct Data {
    que: UnsafeSpsc,
}

pub struct Ref<'a>(Option<UnsafeRef<'a>>, *mut Option<Guard>);

impl<'a> Ref<'a> {
    #[inline]
    pub fn is<T: Any + Copy>(&self) -> bool {
        self.0.as_ref().unwrap().is::<T>()
    }
    #[inline]
    pub fn into<T: Any + Copy>(mut self) -> T {
        self.0.take().unwrap().into::<T>()
    }
}

impl<'a> Drop for Ref<'a> {
    fn drop(&mut self) {
        unsafe {
            // The `UnsafeRef` already guarentees that receiver is alive and that it has only on
            // reference. So this should be safe.
            (*self.1).take();
        }
    }
}

unsafe impl Send for Sender {}
unsafe impl Send for Receiver {}

pub struct Sender(Arc<Data>);

pub struct Receiver {
    data: Arc<Data>,
    g: UnsafeCell<Option<Guard>>,
}


pub fn que() -> (Sender, Receiver) {
    let res = Arc::new(Data {
        que: UnsafeSpsc::new(),
    });
    (Sender(res.clone()),
     Receiver {
        data: res,
        g: UnsafeCell::new(None),
    })
}

impl Sender {
    pub fn send<T: Any + Copy>(&mut self, t: T) {
        unsafe {
            self.0.que.push(t);
        }
    }

    pub fn connected(&self) -> bool {
        Arc::strong_count(&self.0) != 1
    }
}

impl Receiver {
    pub fn recv<'a>(&'a mut self) -> Ref<'a> {
        unsafe {
            loop {
                if self.data.que.is_empty() {
                    continue;
                };
                (*self.g.get()) = Some(epoch::pin());
                let res = self.data.que.pop((*self.g.get()).as_ref().unwrap());
                let g = self.g.get();
                if res.is_some() {
                    return Ref(Some(res.unwrap()), g);
                }
            }
        }
    }

    pub fn try_recv<'a>(&'a mut self) -> Option<Ref<'a>> {
        unsafe {
            if !self.data.que.is_empty() {
                (*self.g.get()) = Some(epoch::pin());
                let res = self.data.que.pop((*self.g.get()).as_ref().unwrap());
                let g = self.g.get();
                res.map(move |x| Ref(Some(x), g))
            } else {
                None
            }
        }
    }

    pub fn connected(&self) -> bool {
        Arc::strong_count(&self.data) != 1
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[derive(Copy,Clone)]
    struct One {
        foo: u8,
    }

    #[derive(Copy,Clone)]
    struct Two {
        foo: u16,
    }

    #[derive(Copy,Clone)]
    struct Three {
        foo: u32,
    }

    #[derive(Copy,Clone)]
    struct Four {
        foo: u64,
    }

    #[derive(Copy,Clone)]
    struct Five {
        foo: u64,
        bar: u64,
    }

    impl One {
        fn new() -> Self {
            One { foo: 0xde }
        }

        fn correct(&self) {
            if self.foo != 0xde {
                panic!();
            }
        }
    }

    impl Two {
        fn new() -> Self {
            Two { foo: 0xb00b }
        }

        fn correct(&self) {
            if self.foo != 0xb00b {
                panic!();
            }
        }
    }

    impl Three {
        fn new() -> Self {
            Three { foo: 0xdeadbeef }
        }

        fn correct(&self) {
            if self.foo != 0xdeadbeef {
                panic!();
            }
        }
    }

    impl Four {
        fn new() -> Self {
            Four { foo: 0xdeadbeefb00b1e55 }
        }

        fn correct(&self) {
            if self.foo != 0xdeadbeefb00b1e55 {
                panic!();
            }
        }
    }

    impl Five {
        fn new() -> Self {
            Five {
                foo: 0xdeadbeefdeadbeef,
                bar: 0xb00b1e55deadbeef,
            }
        }

        fn correct(&self) {
            if self.foo != 0xdeadbeefdeadbeef || self.bar != 0xb00b1e55deadbeef {
                panic!();
            }
        }
    }

    #[test]
    fn test_one() {
        let (mut s, mut r) = que();
        s.send(Five::new());
        r.try_recv().unwrap().into::<Five>().correct();
    }

    #[test]
    fn test_liniar() {
        let (mut s, mut r) = que();
        for i in 0..1000 {
            match i % 5 {
                0 => s.send(One::new()),
                1 => s.send(Two::new()),
                2 => s.send(Three::new()),
                3 => s.send(Four::new()),
                4 => s.send(Five::new()),
                _ => unreachable!(),
            }
        }

        for i in 0..1000 {
            let res = r.try_recv().unwrap();
            match i % 5 {
                0 => res.into::<One>().correct(),
                1 => res.into::<Two>().correct(),
                2 => res.into::<Three>().correct(),
                3 => res.into::<Four>().correct(),
                4 => res.into::<Five>().correct(),
                _ => unreachable!(),
            }
        }
    }

    #[test]
    fn test_parralel() {
        use std::thread;

        let (mut s, mut r) = que();
        let thread = thread::spawn(move || {
            for i in 0..1000 {
                let res = r.recv();
                match i % 5 {
                    0 => res.into::<One>().correct(),
                    1 => res.into::<Two>().correct(),
                    2 => res.into::<Three>().correct(),
                    3 => res.into::<Four>().correct(),
                    4 => res.into::<Five>().correct(),
                    _ => unreachable!(),
                }
            }
        });


        for i in 0..1000 {
            match i % 5 {
                0 => s.send(One::new()),
                1 => s.send(Two::new()),
                2 => s.send(Three::new()),
                3 => s.send(Four::new()),
                4 => s.send(Five::new()),
                _ => unreachable!(),
            }
        }
        thread.join().unwrap();
    }

    /*
    fn stress_test() {
        use std::thread;

        let (mut s, mut r) = que();
        let thread = thread::spawn(move || {
            for _ in 0..100000 {
                for i in 0..1000 {
                    match i % 5 {
                        0 => s.send(One::new()),
                        1 => s.send(Two::new()),
                        2 => s.send(Three::new()),
                        3 => s.send(Four::new()),
                        4 => s.send(Five::new()),
                        _ => unreachable!(),
                    }
                }
            }
        });

        for i in 0..100000 {
            if i % 1000 == 0 {
                println!("working: {}%", i / 1000);
            }
            for j in 0..1000 {
                match j % 5 {
                    0 => r.recv().is::<One>(),
                    1 => r.recv().is::<Two>(),
                    2 => r.recv().is::<Three>(),
                    3 => r.recv().is::<Four>(),
                    4 => r.recv().is::<Five>(),
                    _ => unreachable!(),
                };
            }
        }

        thread.join().unwrap();
    }

    fn wrapping_resize_test() {
        let (mut s, mut r) = que();
        for i in 0..20 {
            match i % 5 {
                0 => s.send(One::new()),
                1 => s.send(Two::new()),
                2 => s.send(Three::new()),
                3 => s.send(Four::new()),
                4 => s.send(Five::new()),
                _ => unreachable!(),
            }
        }

        for i in 0..20 {
            let res = r.recv();
            match i % 5 {
                0 => res.into::<One>().correct(),
                1 => res.into::<Two>().correct(),
                2 => res.into::<Three>().correct(),
                3 => res.into::<Four>().correct(),
                4 => res.into::<Five>().correct(),
                _ => unreachable!(),
            }
        }

        for i in 0..39 {
            match i % 5 {
                0 => s.send(One::new()),
                1 => s.send(Two::new()),
                2 => s.send(Three::new()),
                3 => s.send(Four::new()),
                4 => s.send(Five::new()),
                _ => unreachable!(),
            }
        }

        for i in 0..39 {
            let res = r.recv();
            match i % 5 {
                0 => res.into::<One>().correct(),
                1 => res.into::<Two>().correct(),
                2 => res.into::<Three>().correct(),
                3 => res.into::<Four>().correct(),
                4 => res.into::<Five>().correct(),
                _ => unreachable!(),
            }
        }
    }
    */
}
