//!
//! Hi There, 
//! You are here because the que is still bugged right.
//! You could have seen it coming, It was inevitable.
//! But after working for an entire week on a single que you just wanted to finish it.
//! This is what happens.
//! You already found a critical bug after you started using it.
//! There are still many to come.
//! Hope you didn't build to much functionality on this que.
//! This is your curse.
//!
//!
//!
//! this module holds the implementation of event_stream.
//! A scsp que which can hold a object of any type. In this case only events.  The que works by storing a typeid and a size before the actual event.
//!
//! ```text
//! problems:
//!
//!     * how do we deal with events which would be fitted on de edge.
//!         - How about we just place the event after the the edge.
//!           if possible we place the head before, the head still dictates the normal size.
//!           If it is not possible to place the head we place the head after the edge.
//!           This is a algoritme which can make the place of events apperent for both clients.
//!
//!     * How do we deal with buffer overflow?
//!         - When the buffer overflows the source copies the buffer to a new buffer and then swaps
//!           the old buffer with the new one. Old one is properly handled by epoch.
//!
//!     * How do we make push possible without resorting to closures.
//!         - It might be possible to keep the gaurd from epoch in the EventRef.
//!
//!     * How do we deal with alligment.
//!         All events are alligned on the largest alligment.
//!
//! # Importent!!
//!
//!     *  Back and front should just continue growing. Making it 0 again.
//!
//! # Possible conditions
//!
//!  registery:
//!
//!     H : Header data
//!     E : Event data
//!
//!  1. new event fits
//!
//!   new event is placed after the back.
//!    before:
//!  .____________________________________________________________________________.
//!  |                    |  various events and headers            |              |
//!  .--------------------^----------------------------------------^--------------.
//!                     front                                     back
//!    after:
//!  .____________________________________________________________________________.
//!  |                    |  various events and headers            |H|EEE|        |
//!  .--------------------^----------------------------------------------^--------.
//!                     front                                           back
//!
//!  1. new event only header fits.
//!
//!   header is placed with a a size 0 and a typeid of the header.
//!   this is to indicate that the event didnt fit and that
//!    before:
//!  ._____________________________________________________________________________.
//!  |                    |  various events and headers                          | |
//!  .--------------------^------------------------------------------------------^-.
//!                     front                                                   back
//!    after:
//!  ._____________________________________________________________________________.
//!  |H|EEE|              |  various events and headers                          |H|
//!  .-----^--------------^--------------------------------------------------------.
//!       back          front
//!
//!  1. header doesnt fit either
//!     this should not be possible
//!     When pushing events the pusher should check if there is still place for a headers.
//!     If not the event should be pushed as if there is no place for the vent only.
//!
//! # resize
//!     before:
//! ._____________________________________.
//! |EEEE|                     |EEEEEEEEEE|
//! .----^---------------------^----------.
//!     back                 front
//!
//!     after:                         old edge
//! .__________________________________________________________________________.
//! |                          |EEEEEEEEEE|EEEE|                               |
//! .--------------------------^---------------^-------------------------------.
//!                          front            back
//!
//! Also needs to handle different situations at the end of the
//! ```

use ::crossbeam::epoch::{self, Atomic, Owned, Guard};
use std::sync::atomic::{Ordering, AtomicUsize};
use std::cell::UnsafeCell;
use std::mem;
use std::ptr;
use std::any::{Any, TypeId};

// must be a multiple of 2 in order to wrap arround correctly.
const START_SIZE: usize = 1024;

unsafe fn alloc_array(size: usize) -> Box<[u8]> {
    let mut vec = Vec::new();
    vec.reserve_exact(size);
    vec.set_len(size);
    for i in 0..(size/8){
        let d: &mut u64 = mem::transmute(&mut vec[i * 8]);
        *d = mem::transmute(['h', 'e']);
    }
    vec.into_boxed_slice()
}

struct Header {
    id: TypeId,
    size: usize,
}

impl Header{
    fn invalid(size: usize) -> Self{
        Header{
            id: TypeId::of::<Header>(),
            size: size,
        }
    }

    fn is_invalid(&self) -> bool{
        self.id == TypeId::of::<Header>()
    }
}

pub struct Ref<'a>{
    head: &'a Header,
    new: usize,
    front: &'a AtomicUsize,
}

impl<'a> Ref<'a>{
    pub fn is<T: Any + Copy>(&self) -> bool{
        self.head.id == TypeId::of::<T>()
    }

    pub fn into<T: Any + Copy>(self) -> T{
        if !self.is::<T>() {
            panic!("Tried to convert type into wrong type");
        }
        unsafe{
            let h: *mut HeaderAndData<T> = mem::transmute(self.head);
            (*h).data
        }
    }
}

impl<'a> Drop for  Ref<'a>{
    fn drop(&mut self){
        self.front.fetch_add(self.new,Ordering::AcqRel);
    }
}

pub struct UnsafeSpsc {
    buffer: Atomic<UnsafeCell<Box<[u8]>>>,
    // can only be changed by writer.
    front: AtomicUsize,
    // can only be changed by reader
    back: AtomicUsize,

    size_hint: AtomicUsize,
}

#[repr(C)]
struct HeaderAndData<T>{
    head: Header,
    data: T
}

impl UnsafeSpsc {
    pub fn new() -> Self {
        let buffer = Atomic::null();

        unsafe{
            let buf = alloc_array(START_SIZE);
            buffer.store(Some(Owned::new(UnsafeCell::new(buf))),Ordering::Release);
        }
        UnsafeSpsc {
            buffer: buffer,
            front: AtomicUsize::new(0),
            back: AtomicUsize::new(0),
            size_hint: AtomicUsize::new(START_SIZE),
        }
    }

    pub unsafe fn push<T: Any + Copy>(&self, t: T) {
        let size_h = mem::size_of::<Header>();
        let size_full = mem::size_of::<HeaderAndData<T>>();
        let gaurd = epoch::pin();

        loop{
            let borrow = self.buffer.load(Ordering::Acquire, &gaurd)
                .expect("Who took my buffer and left null?")
                .get();

            let len = (*borrow).len();
            let back = self.back.load(Ordering::Acquire);
            let back_wrapped = back % len;
            let front = self.front.load(Ordering::Acquire);
            let front_wrapped = front % len;

            // size of the lenght of used buffer.
            let curr_len = if back_wrapped >= front_wrapped{
                back_wrapped - front_wrapped
            }else{
                len - (front_wrapped - back_wrapped)
            };

            let new_len = curr_len + size_full;

            //check if we need to resize.
            //we add the size of the head to ensure that when data doesnt fit we can always write
            //the header. This also makes sure that the que is not precieved a empty even though it
            //is completely full because front and back are the same.
            if new_len + size_h > len{
                self.resize();
                continue;
            }

            // check if we can write the data and an header to ensure that if data doesnt fit we
            // can write an invalid header.
            if back_wrapped + size_full + size_h > len {
                // we cant fit our data so write an invalid header.
                let borrow: *mut Header = mem::transmute((*borrow).as_mut_ptr().offset(back_wrapped as isize));
                let size = len - back_wrapped;
                ptr::write(borrow,Header::invalid(size));
                // can we wrap?
                self.back.store(back + (len - back_wrapped),Ordering::Release);
                continue;
            }
            // conditions okay
            let borrow: *mut HeaderAndData<T> = mem::transmute((*borrow).as_mut_ptr().offset(back_wrapped as isize));
            ptr::write(borrow,HeaderAndData{
                head: Header{
                    id: TypeId::of::<T>(),
                    size: size_full,
                },
                data: t,
            });
            self.back.store(back + size_full,Ordering::Release);
            return;
        }
    }

    /// cannot be called in parralel with other resizes.
    unsafe fn resize(&self) {
        let gaurd = epoch::pin();
        let borrow = self.buffer.load(Ordering::Acquire, &gaurd)
            .expect("Null?? i'll null your face!")
            .get();

        let len = (*borrow).len();
        //times two so it stays a multiple of 2.
        let new_len = len*2;

        let front = self.front.load(Ordering::Acquire);
        let front_wrapped = front % len;
        let back = self.back.load(Ordering::Acquire);
        let back_wrapped = back % len;
        let mut new_buffer = alloc_array(new_len);

        if front_wrapped < back_wrapped {
            ptr::copy((*borrow).as_ptr().offset(front_wrapped as isize)
                      ,new_buffer.as_mut_ptr().offset(front_wrapped as isize)
                      ,back_wrapped - front_wrapped);
            self.back.store(back_wrapped,Ordering::Release);
        } else {

            // copy front to edge.
            ptr::copy((*borrow).as_ptr().offset(front_wrapped as isize)
                      ,new_buffer.as_mut_ptr().offset(front_wrapped as isize)
                      ,len - front_wrapped);

            // copy edge to back after last.
            ptr::copy((*borrow).as_ptr()
                      ,new_buffer.as_mut_ptr().offset(len as isize)
                      ,back_wrapped);

            self.back.store(back_wrapped + len,Ordering::Release);
        }
        //wrap front with change.
        self.front.fetch_sub(front - front_wrapped,Ordering::AcqRel);
        self.buffer.store(Some(Owned::new(UnsafeCell::new(new_buffer))),Ordering::Release);
        self.size_hint.store(new_len,Ordering::Release);
    }

    pub fn is_empty(&self) -> bool{
        let len = self.size_hint.load(Ordering::Acquire);
        self.front.load(Ordering::Acquire) % len == self.back.load(Ordering::Acquire)
    }

    /// cannot be called in parralel with other pops
    pub unsafe fn pop<'a>(&'a self,gaurd: &'a Guard) -> Option<Ref<'a>>{
        let borrow = self.buffer.load(Ordering::Acquire, gaurd)
            .expect("Yack Nullculture!")
            .get();

        let len = (*borrow).len();

        let front = self.front.load(Ordering::Acquire);
        let front_wrapped = front % len;
        let back_wrapped = self.back.load(Ordering::Acquire) % len;

        if front_wrapped == back_wrapped{
            return None;
        }

        let mut value: &Header = mem::transmute((*borrow).as_ptr().offset(front_wrapped as isize));

        let mut next = 0;
        while value.is_invalid(){
            next += value.size;
            value = mem::transmute((*borrow).as_ptr().offset((front % len) as isize));
            if front % len == back_wrapped{
                return None;
            }
        }

        Some(Ref{
            head: value,
            new: next + value.size,
            front: &self.front,
        })
    }
}

