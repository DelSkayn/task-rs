mod unsafe_spsc;

use self::unsafe_spsc::UnsafeSpsc;

use std::sync::Arc;

struct Data<T: Send> {
    que: UnsafeSpsc<T>,
}

unsafe impl<T: Send> Send for Sender<T> {}
unsafe impl<T: Send> Send for Receiver<T> {}

pub struct Sender<T: Send>(Arc<Data<T>>);

pub struct Receiver<T: Send>(Arc<Data<T>>);


pub fn que<T: Send>() -> (Sender<T>, Receiver<T>) {
    let res = Arc::new(Data {
        que: UnsafeSpsc::new(),
    });
    (Sender(res.clone()), Receiver(res))
}

impl<T: Send> Sender<T> {
    pub fn send(&mut self, t: T) {
        unsafe {
            self.0.que.push(t);
        }
    }

    pub fn connected(&self) -> bool {
        Arc::strong_count(&self.0) != 1
    }
}

impl<T: Send> Receiver<T> {
    pub fn recv(&mut self) -> T {
        unsafe {
            loop {
                if let Some(x) = self.0.que.pop() {
                    return x;
                }
            }
        }
    }

    pub fn try_recv(&mut self) -> Option<T> {
        unsafe {
            if !self.0.que.is_empty_hint() {
                self.0.que.pop()
            } else {
                None
            }
        }
    }

    pub fn connected(&self) -> bool {
        Arc::strong_count(&self.0) != 1
    }
}

impl<T: Send + PartialEq> Receiver<T> {
    pub fn is(&self, t: T) {
        unsafe {
            self.0.que.is(t);
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;

    struct Foo {
        foo: u32,
    }

    #[test]
    fn test_one() {
        let (mut s, mut r) = que();
        s.send(Foo { foo: 0xdeadbeef });
        assert!(r.try_recv().unwrap().foo == 0xdeadbeef);
    }

    #[test]
    fn test_liniar() {
        let (mut s, mut r) = que();
        for i in 0..1000 {
            s.send(Foo { foo: i });
        }

        for i in 0..1000 {
            let res = r.try_recv().unwrap();
            assert!(res.foo == i);
        }
    }

    #[test]
    fn test_parralel() {
        use std::thread;

        let (mut s, mut r) = que();
        let thread = thread::spawn(move || {
            for i in 0..1000 {
                assert!(r.recv() == i);
            }
        });


        for i in 0..1000 {
            s.send(i);
        }
        thread.join().unwrap();
    }

    /*
    fn stress_test() {
        use std::thread;

        let (mut s, r) = que();
        let thread = thread::spawn(move || {
            for _ in 0..100000 {
                for i in 0..1000 {
                    s.send(i);
                }
            }
        });

        for i in 0..100000 {
            if i % 1000 == 0 {
                println!("working: {}%", i / 1000);
            }
            for j in 0..1000 {
                r.is(j);
            }
        }

        thread.join().unwrap();
    }
    */
}
