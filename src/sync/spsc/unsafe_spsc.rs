//! This module defines the unsafe implementation of the spsc which implemented with a resizable
//! circular buffer. This is faster then the mpsc in the standart library because it does not need
//! to do an allocation for each pushed object.
//!

use ::crossbeam::epoch::{self, Atomic, Owned};
use std::sync::atomic::{Ordering, AtomicUsize};
use std::cell::UnsafeCell;
use std::ptr;

// must be a multiple of 2 in order to wrap arround correctly.
const START_SIZE: usize = 8;

unsafe fn alloc_array<T: Send>(size: usize) -> Box<[T]> {
    let mut vec = Vec::new();
    vec.reserve_exact(size);
    vec.set_len(size);
    vec.into_boxed_slice()
}

pub struct UnsafeSpsc<T: Send> {
    buffer: Atomic<UnsafeCell<Box<[T]>>>,
    // can only be changed by writer.
    front: AtomicUsize,
    // can only be changed by reader
    back: AtomicUsize,
    size_hint: AtomicUsize,
}

impl<T: Send> UnsafeSpsc<T> {
    pub fn new() -> Self {
        let buffer = Atomic::null();
        unsafe {
            let buf = alloc_array::<T>(START_SIZE);
            buffer.store(Some(Owned::new(UnsafeCell::new(buf))), Ordering::Release);
        }
        UnsafeSpsc {
            buffer: buffer,
            front: AtomicUsize::new(0),
            back: AtomicUsize::new(0),
            size_hint: AtomicUsize::new(START_SIZE),
        }
    }

    pub unsafe fn push(&self, t: T) {
        let gaurd = epoch::pin();

        loop {
            let borrow = self.buffer
                .load(Ordering::Acquire, &gaurd)
                .expect("Who took my buffer and left null?")
                .get();

            let len = (*borrow).len();
            let back = self.back.load(Ordering::Acquire);
            let back_wrapped = back % len;
            let front = self.front.load(Ordering::Acquire);
            let front_wrapped = front % len;

            assert!(((back as isize) - (front as isize)).abs() as usize <= len);

            // size of the lenght of used buffer.
            let curr_len = if back_wrapped >= front_wrapped {
                back_wrapped - front_wrapped
            } else {
                len - (front_wrapped - back_wrapped)
            };

            let new_len = curr_len + 1;

            if new_len == len {
                self.resize();
                continue;
            }

            // conditions okay
            ptr::write((*borrow).as_mut_ptr().offset(back_wrapped as isize), t);
            self.back.store(back.wrapping_add(1), Ordering::Release);
            return;
        }
    }

    /// cannot be called in parralel with other resizes.
    unsafe fn resize(&self) {
        let gaurd = epoch::pin();
        let borrow = self.buffer
            .load(Ordering::Acquire, &gaurd)
            .expect("Null?? i'll null your face!")
            .get();

        let len = (*borrow).len();
        // times two so it stays a multiple of 2.
        let new_len = len * 2;

        let front = self.front.load(Ordering::Acquire);
        let front_wrapped = front % len;
        let back = self.back.load(Ordering::Acquire);
        let back_wrapped = back % len;
        let mut new_buffer = alloc_array(new_len);

        if front_wrapped < back_wrapped {
            ptr::copy_nonoverlapping((*borrow).as_ptr().offset(front_wrapped as isize),
                                     new_buffer.as_mut_ptr().offset(front_wrapped as isize),
                                     back_wrapped - front_wrapped);
            self.back.store(back_wrapped, Ordering::Release);
        } else {

            // copy front to edge.
            ptr::copy_nonoverlapping((*borrow).as_ptr().offset(front_wrapped as isize),
                                     new_buffer.as_mut_ptr().offset(front_wrapped as isize),
                                     len - front_wrapped);

            // copy edge to back after last.
            ptr::copy_nonoverlapping((*borrow).as_ptr(),
                                     new_buffer.as_mut_ptr().offset(len as isize),
                                     back_wrapped);

            self.back.store(back_wrapped + len, Ordering::Release);
        }
        // wrap front with change.
        self.front.fetch_sub(front - front_wrapped, Ordering::AcqRel);
        self.buffer.store(Some(Owned::new(UnsafeCell::new(new_buffer))),
                          Ordering::Release);
        self.size_hint.store(new_len, Ordering::Release);
    }

    pub fn is_empty_hint(&self) -> bool {
        let len = self.size_hint.load(Ordering::Acquire);
        self.front.load(Ordering::Acquire) % len == self.back.load(Ordering::Acquire)
    }

    /// cannot be called in parralel with other pops
    pub unsafe fn pop(&self) -> Option<T> {
        let guard = epoch::pin();
        let borrow = self.buffer
            .load(Ordering::Acquire, &guard)
            .expect("Yack Nullculture!")
            .get();

        let len = (*borrow).len();

        let front = self.front.load(Ordering::Acquire);
        let front_wrapped = front % len;
        let back_wrapped = self.back.load(Ordering::Acquire) % len;

        if front_wrapped == back_wrapped {
            return None;
        }

        let res = ptr::read((*borrow).as_ptr().offset(front_wrapped as isize));
        self.front.fetch_add(1, Ordering::AcqRel);
        Some(res)
    }
}

impl<T: Send + PartialEq> UnsafeSpsc<T> {
    pub unsafe fn is(&self, t: T) {
        loop {
            let guard = epoch::pin();
            let borrow = self.buffer
                .load(Ordering::Acquire, &guard)
                .expect("Yack Nullculture!")
                .get();

            let len = (*borrow).len();

            let front = self.front.load(Ordering::Acquire);
            let front_wrapped = front % len;
            let back_wrapped = self.back.load(Ordering::Acquire) % len;

            if front_wrapped == back_wrapped {
                continue;
            }

            let res = ptr::read((*borrow).as_ptr().offset(front_wrapped as isize));
            assert!(res == t);
            self.front.fetch_add(1, Ordering::AcqRel);
            return;
        }
    }
}
