//! A buffer which can both be read from and writen to at the same time.
//! Front is write only, back is read only,
//! Rust cannot prevent reads from something which is mutable, so you can still read from front.
//! However reads are only consistent in between swaps.
//! If you write to the front, swap and try to read the value written it there is no gaurantee it
//! will be the same.

use rt::worker;

use std::cell::UnsafeCell;
use std::sync::Arc;
use std::sync::atomic::{Ordering, AtomicUsize};
use std::ops::{DerefMut, Deref};

struct DoubleBuffer<T: Sized> {
    // which on is the front buffer and which on is the back buffer.
    // and wether the back is currently reading.
    //
    // 0 => mutable is buffers.0 and other buffer is not borrowed
    // 1 => mutable is buffers.0 and other buffer is borrowed
    // 2 => mutable is buffers.1 and other buffer is not borrowed
    // 3 => mutable is buffers.1 and other buffer is borrowed
    //
    marker: AtomicUsize,
    // the buffers, might need to add a cache fill block.
    buffers: (UnsafeCell<T>, UnsafeCell<T>),
}

pub fn double_buffer<T: Sized>(front: T, back: T) -> (Front<T>,Back<T>) {
    let buf = Arc::new(DoubleBuffer {
        marker: AtomicUsize::new(0),
        buffers: (UnsafeCell::new(front), UnsafeCell::new(back)),
    });
    (Front(buf.clone()), Back(buf))
}

/// The write only part of the DoubleBuffer.
/// Rust can not prevent a read from a value so you can still read from the buffer.
/// But reads from the front buffer are only consistent between swapping.
/// Also in charge of swapping the buffers to present changes to front.
///
/// This struct implements deref.
/// But this implemention requires a atomic load and a non trivial match.
/// It is thus recommended to store the result of a deref when many accesses are required in
/// performance critical code.
pub struct Front<T: Sized>(Arc<DoubleBuffer<T>>);

unsafe impl<T: Sync> Send for Front<T>{}
unsafe impl<T: Sync> Sync for Front<T>{}

unsafe impl<T: Sync> Send for Back<T>{}
unsafe impl<T: Sync> Sync for Back<T>{}

impl<T: Sized> Front<T> {
    /// Swap back and front buffer.
    pub fn swap(&mut self) {
        if self.back_present() {
            // back left so no need to do difficult.
            let current = self.0.marker.load(Ordering::Acquire);
            let new = if current != 0 { 0 } else { 2 };
            self.0.marker.store(new,Ordering::Release);
            return;
        }
        while !self.try_swap() {
            worker::work();
        }
    }

    /// Swap the front and back buffer.
    /// Returns wether the swap was succesfull.
    pub fn try_swap(&mut self) -> bool {
        let current = self.0.marker.load(Ordering::Acquire);
        if current % 2 == 1 {
            return false;
        }
        let new = if current != 0 { 0 } else { 2 };
        self.0.marker.compare_and_swap(current, new, Ordering::AcqRel) == current
    }

    /// Returns wether the back buffer is still present.
    /// Returns false when the back is dropped.
    #[inline]
    pub fn back_present(&self) -> bool {
        Arc::strong_count(&self.0) != 1
    }
}

impl<T: Sized> Deref for Front<T> {
    type Target = T;

    fn deref(&self) -> &Self::Target {
        let which = self.0.marker.load(Ordering::Acquire);
        match which {
            0 | 1 => unsafe { &(*self.0.buffers.0.get()) },
            2 | 3 => unsafe { &(*self.0.buffers.1.get()) },
            _ => unreachable!(),
        }
    }
}

impl<T: Sized> DerefMut for Front<T> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        let which = self.0.marker.load(Ordering::Acquire);
        match which {
            0 | 1 => unsafe { &mut (*self.0.buffers.0.get()) },
            2 | 3 => unsafe { &mut (*self.0.buffers.1.get()) },
            _ => unreachable!(),
        }
    }
}

/// Gaurd used to acces the data in the back buffer.
pub struct BackGaurd<'a, T: Sized + 'a> {
    marker: &'a AtomicUsize,
    borrow: &'a T,
}

impl<'a, T: Sized> Drop for BackGaurd<'a, T> {
    fn drop(&mut self) {
        // rest marker.
        self.marker.fetch_sub(1, Ordering::AcqRel);
    }
}

impl<'a, T: Sized> Deref for BackGaurd<'a, T> {
    type Target = T;

    fn deref(&self) -> &Self::Target {
        self.borrow
    }
}


/// The read only part of the Doublebuffer.
pub struct Back<T: Sized>(Arc<DoubleBuffer<T>>);

impl<T: Sized> Back<T> {
    /// borrow the back buffer for reading.
    /// reads are consistent while holding the gaurds.
    /// however reads might change between calls to this function.
    pub fn borrow<'a>(&'a mut self) -> BackGaurd<'a, T> {
        // mark as borrowed
        let which = self.0.marker.fetch_add(1, Ordering::AcqRel);
        let borrow = match which {
            0 => unsafe { &(*self.0.buffers.1.get()) },
            2 => unsafe { &(*self.0.buffers.0.get()) },
            _ => unreachable!(),
        };
        BackGaurd {
            marker: &self.0.marker,
            borrow: borrow,
        }
    }

    /// Returns wether the front buffer is still present.
    /// Returns false when the front is dropped.
    #[inline]
    pub fn front_present(&self) -> bool {
        Arc::strong_count(&self.0) != 1
    }
}
