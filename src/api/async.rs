use task::{Task,TaskAtomic};

use rt::worker;
use rt::que::StealQue;

use std::cell::Cell;
use std::marker::PhantomData;

// READ BEFORE YOU CHANGE THIS
//
// 


/// StackAsync presents a task to the schedular for execution.
/// If no other thread was able to execute the task it is executed inline once the object is
/// dropped.
/// StackAsync does not use heap allocation is located on the stack.
/// This is why the function run must be called on the object in order to lock the object in its 
/// place on the stack. This also means that once run is called the object can no longer move.
pub struct StackAsync<'a, F>
    where F: FnOnce() + Send
{
    task: Option<TaskAtomic<F, ()>>,
    _marker: Cell<PhantomData<&'a ()>>,
}

impl<'a, F> StackAsync<'a, F>
    where F: FnOnce() + Send
{
    pub fn new(func: F) -> Self {
        unsafe {
            StackAsync{
                task: Some(TaskAtomic::new(func)),
                _marker: Cell::new(PhantomData),
            }
        }
    }

    /// Present the task to the schedular for execution.
    pub fn run(&'a self) {
        unsafe {
            StealQue::push(self.task.as_ref().unwrap().as_task_ref());
        }
        self._marker.set(PhantomData);
    }
}

impl<'a, F> Drop for StackAsync<'a, F>
    where F: FnOnce() + Send
{
    fn drop(&mut self) {
        unsafe {
            // make sure the pushed and popped tasks are the same.
            let temp = StealQue::pop();
            if cfg!(debug_assertions) && temp.is_some() {
                if temp.as_ref().unwrap().clone() != self.task.as_ref().unwrap().as_task_ref() {
                    panic!("Mismatched task pop.");
                }
            }

            if temp.is_some() {
                self.task.take().unwrap().execute_inline();
            } else {
                {
                    let borrow = self.task.as_ref().unwrap();
                    while !borrow.done() {
                        worker::work();
                    }
                }
                self.task.take().unwrap().into_result();
            }
        }
    }
}
