use ::task::{AsTaskRef,Task,TaskAtomic};
use ::task::task_arena::TaskArena;
use ::task::local_task::LocalTask;
use std::cell::Cell;

//use super::Future;

use std::marker::PhantomData;

use std::ptr;

use rt::que::GlobalQue;
use rt::worker;

pub trait Promises<R>{
    fn done(&self) -> bool;

    fn get(&self) -> R;
}

impl<'a,F,R> Promises<R> for Promise<'a,F,R>
    where F: FnOnce() -> R + Send + 'a
{
    fn done(&self) -> bool{
        self.done()
    }

    fn get(&self) -> R{
        self.get()
    }
}


// Do i need the function type in the struct or can i maybe move that
// to the new function.
pub struct Promise<'a, F, R>
    where F: FnOnce() -> R + Send + 'a
{
    task: Cell<Option<*mut LocalTask>>,
    _marker: PhantomData<(&'a F,R)>,
}

unsafe impl<'a,F, R> Send for Promise<'a,F, R>
    where F: FnOnce() -> R + Send + 'a{}

impl<'a, F, R> Promise<'a, F, R>
    where F: FnOnce() -> R + Send + 'a
{
    pub fn new(func: F) -> Self {
        unsafe {
            let task = TaskAtomic::new(func);
            let ptr = TaskArena::alloc();
            ptr::write(ptr,LocalTask::new(task));
            GlobalQue::push((*ptr).as_task_ref());
            Promise {
                task: Cell::new(Some(ptr)),
                _marker: PhantomData,
            }
        }
    }

    pub fn done(&self) -> bool {
        unsafe {
            (*self.task.get()
                .expect("Tried to check if promise was done after result already returned"))
                .downcast::<TaskAtomic<F,R>>().done()
        }
    }

    pub fn get(&self) -> R {
        unsafe {
            let borrow = (*self.task.get()
                .expect("Tried to check if promise was done after result already returned"))
                .downcast::<TaskAtomic<F,R>>();
            while !borrow.done(){
                worker::work();
            }
            let res = borrow.into_result();
            TaskArena::dealloc(self.task.get()
                .expect("Tried to check if promise was done after result already returned"));
            self.task.set(None);
            res
        }
    }

    /*
    pub fn then<'b: 'a,A,T>(self,f: T) -> Promise<'b,impl FnOnce() -> A + Send + 'b,A>
        where T: FnOnce(R) -> A + Send + 'b
    {

        Promise::new(move ||{
            f(self.get())
        })
    }
    */
}

impl<'a, F, R> Drop for Promise<'a, F, R>
    where F: FnOnce() -> R + Send + 'a
{
    fn drop(&mut self) {
        unsafe {
            self.task.get().map(|e|{
                let borrow = (*e).downcast::<TaskAtomic<F,R>>();
                while !borrow.done(){
                    worker::work();
                }
                borrow.into_result();
                TaskArena::dealloc(e);
            });
        }
    }
}

/*
impl<'a, F, R> Future for Promise<'a, F, R>
    where F: FnOnce() -> R + Send + 'a,
{
    type Item = R;

    fn procede(self) -> R{
        self.get()
    }

    fn ready(&mut self) -> bool{
        self.done()
    }
*/
