use ::rt::que::{GlobalQue,StealQue};
use ::rt::{worker,Runtime};
use ::task::{Task,TaskAtomic,TaskArena,LocalTask,AsTaskRef};
use ::rt::sleep::Sleep;
use std::ptr;

pub use ::rt::JoinHandle;

pub mod async;
pub mod promise;

/// # !!Idea!!
/// Have an async take an optional value with a longer livetime.
/// Place the value the function returns inside that value.
/// This allows the async to be dropped in the right order while still returning a value

/// Executes the two given functions asyncronous.
/// Usefull for paralizing algoritmes which can be solved by devide and conquer.
///
/// Join always executes the first function on the local thread. The Second function is pushed to a
/// que. If no other thread was able to execute the second function it while the first was being
/// executed the second is also executed localy. If a thread is executing the second function the
/// thread will try to steal work until the second function is finished.
///
/// Please keep in mind that this function can still cause deadlocks in unpredictible ways if
/// syncronisation primitives are used.
///
/// Cannot be called from a user created thread!
///
/// # Example
/// ```
/// fn fibbo(i: usize) -> usize{
///     match i {
///         0 => 1,
///         1 => 1,
///         _ => {
///             let (a,b) = join(|| fibbo(i-1),|| fibbo(i-2)); a+b
///         }
///     }
/// }
/// ```
pub fn join<A,B,AR,BR>(a: A,b: B) -> (AR,BR) where A: FnOnce() -> AR,
          B: FnOnce() -> BR + Send
{

    unsafe{

        // push b to the steal que for possible parralel execution.
        let t_b = TaskAtomic::new(b);
        StealQue::push(t_b.as_task_ref());
        Sleep::global_wake();

        // execute task a immediately
        let a_res = a();

        // make sure that the task popped is actually the task
        // we pushed
        let temp = StealQue::pop();
        if cfg!(debug_assertions) && temp.is_some(){
            if temp.as_ref().unwrap().clone() != t_b.as_task_ref(){
                panic!("Mismatched task pop.");
            }
        }

        // if we poped a task execute it inline.
        // else do work until we can return.
        let b_res = if temp.is_some() {
            t_b.execute_inline()
        }else{
            while !t_b.done(){
                worker::work();
            }
            t_b.into_result()
        };
        (a_res,b_res)
    }
}

/// Struct whose lifetime defines the lenght of a syncronisation.
/// While this object is alive all threads are syncronized.
pub struct SyncGuard;

impl Drop for SyncGuard{
    fn drop(&mut self){
        Runtime::desyncronize();
    }
}

/// Syncronized all threads.
///
/// This function waits untill all threads hit a syncronisation point.
/// During syncronisation all other threads are locked until the SyncGaurd is dropped.
/// This function is mostly used in conjunction with other functions like Thread::spawn.
/// Nested calls to this function are a no-op.
///
/// Keep in mind that syncronisations are expensive. It is thus recommended to avoid calling
/// functions which require a syncronisation.
///
/// All threads also the threads which are created with spawn should contain a call to sync_point
/// which can be reached in while syncronizing. Otherwise this function will never return.
/// # Panic
/// Will panic if the function is called on a thread which is no the main thread.
///
/// # Example
/// ```
/// let guard = sync();
/// spawn(&guard,||{
///     println!("Hello from a task thread!");
///     // I can do some work!()
///     work();
/// });
/// ```
#[inline]
pub fn sync() -> SyncGuard{
    Runtime::syncronize();
    SyncGuard
}

/// Declare a point at which the thread can syncronize if nessecary with other threads.
///
/// This function will cause the thread to hold if a syncronisation was requested until
/// syncronisation is finished.
///
/// Function should be used in user managed threads in order to allow syncronisation to happen.
/// If a thread cannot reach a sync point while syncronisation is requested the application will
/// possibly deadlock, or at least all worker threads and the thread which requested syncronisation
/// will lock forever.
///
/// # Return
/// Whether or not syncronisation took place.
/// # Example
/// ```ignore
/// while running {
///     // Do some work for example render a frame.
///     sync_point();
/// }
/// ```
#[inline]
pub fn sync_point() -> bool {
    Runtime::hit_sync()
}

/// Create a new thread.
///
/// Use this function over `std::thread::spawn`.
///
/// Function will spin up a new system thread which will execute the given function.
/// It should thus only be used if a special thread is required to execute a function.
/// # Example
/// ```
/// # use self::*
///
/// let guard = sync();
/// spawn(&guard,||{
///     println!("Hello from a task thread!");
///     // I can do some work!()
///     work();
/// });
/// ```
#[inline]
pub fn spawn<F: FnOnce() + Send + 'static>(_: &SyncGuard,func: F) -> JoinHandle{
    unsafe { Runtime::new_thread(func) }
}

/// Execute a function on a thread.
pub fn execute<F: FnOnce() + Send + 'static>(func: F){
    unsafe{
        #[derive(Clone,Copy)]
        struct Wrap(*mut LocalTask);
        unsafe impl Send for Wrap{}
        let alloc = Wrap(TaskArena::alloc());
        let task = TaskAtomic::new(move ||{
            func();
            TaskArena::dealloc(alloc.0);
        });
        ptr::write(alloc.0,LocalTask::new(task));
        GlobalQue::push((*alloc.0).as_task_ref());
        Sleep::global_wake();
    }
}

/// Try to do work
///
/// This function tries to retrieve work and then execute said work.
/// # Return
/// Wether or not work was done.
/// True if work was done.
#[inline]
pub fn try_work() -> bool{
    worker::work()
}

/// Start the runtime of task-rs
///
/// By default the runtime is started on the first call which requires the use of the runtime.
/// This function will start the runtime immediately
pub fn start(){
    Runtime::start()
}

