use super::*;

use std::mem;
use std::ptr;
use std::cell::UnsafeCell;

#[repr(C)]
struct RawTrait{
    data: *mut(),
    v_table: *mut(),
}

const LOCAL_TASK_SIZE: usize = 32;

pub struct LocalTask{
    data: UnsafeCell<[*mut (); LOCAL_TASK_SIZE - 1]>,
    v_table: *mut (),
}

impl LocalTask{
    pub fn new<T: Task>(t: T) -> Self{
        if mem::size_of::<T>() > mem::size_of::<[*mut ();LOCAL_TASK_SIZE - 1]>()
            || mem::align_of::<T>() > mem::align_of::<[*mut ();LOCAL_TASK_SIZE - 1]>(){

            unsafe{
                let res = LocalTask{
                    data: mem::uninitialized(),
                    v_table: ptr::null_mut(),
                };
                ptr::write(res.data.get() as *mut *mut Task,Box::into_raw(Box::new(t)));
                res
            }
        }else{
            unsafe{
                let t_raw: RawTrait = {
                    let tr: &Task = &t;
                    mem::transmute(tr)
                };
                let res = LocalTask{
                    data: mem::uninitialized(),
                    v_table: t_raw.v_table,
                };
                ptr::write(res.data.get() as *mut T,t);
                res
            }
        }
    }

    pub unsafe fn downcast<T: Task>(&self) -> &T{
        if self.v_table == ptr::null_mut(){
            panic!("downcast not implemented for heap allocated localtask.");
        }
        mem::transmute(&self.data)
    }
}

impl Drop for LocalTask{
    fn drop(&mut self){
        if self.v_table == ptr::null_mut(){
            unsafe{
                let t: *mut Task = ptr::read(self.data.get() as *mut *mut Task);
                Box::from_raw(t);
            }
        }
    }
}

impl Task for LocalTask{
    unsafe fn execute(&self){
        (*self.as_task_ref().0).execute();
    }

    unsafe fn done(&self) -> bool{
        (*self.as_task_ref().0).done()
    }
}

impl AsTaskRef for LocalTask{
    unsafe fn as_task_ref(&self) -> TaskRef{
        if self.v_table != ptr::null_mut(){
            let t_raw = RawTrait{
                data: self.data.get() as *mut (),
                v_table: self.v_table,
            };
            TaskRef(mem::transmute(t_raw))
        }else{
            let t: *mut Task = ptr::read(self.data.get() as *mut *mut Task);
            TaskRef(t)
        }
    }
}
