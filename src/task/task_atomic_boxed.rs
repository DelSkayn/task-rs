
use super::{Task,  TaskRef, AsTaskRef};

use std::cell::UnsafeCell;
use std::sync::atomic::{Ordering,AtomicBool};
use std::boxed::FnBox;
use std::mem;

pub struct TaskAtomicBoxed<R> {
    sync: AtomicBool,
    func: UnsafeCell<Option<Box<FnBox() -> R + Send>>>,
    result: UnsafeCell<Option<R>>,
}

impl<R> TaskAtomicBoxed<R> {
    pub unsafe fn new<F>(func: F) -> Self
        where F: FnOnce() -> R + Send
    {

        let func: Box<FnBox() -> R + Send> = Box::new(func);
        let static_func: Box<FnBox() -> R + Send + 'static> = mem::transmute(func);
        TaskAtomicBoxed{
            sync: AtomicBool::new(false),
            func: UnsafeCell::new(Some(static_func)),
            result: UnsafeCell::new(None),
        }
    }

    // pub unsafe fn from_box(latch: L,func: Box<FnBox() -> R + Send>) -> Self{
    // let static_func: Box<FnBox() -> R> + Send + 'static = mem::transmute(func);
    // DynTaskImpl{
    // latch: latch,
    // func: UnsafeCell::new(Some(static_func)),
    // result:UnsafeCell::new(TaskResult::None),
    // }
    // }
    //

    pub unsafe fn execute_inline(self) -> R {
        let func = self.func.into_inner().unwrap();
        let func_cor: Box<FnBox() -> R> = mem::transmute(func);
        func_cor.call_box(())
    }

    pub unsafe fn as_task_ref(&self) -> TaskRef {
        let task_ref: *const (Task + 'static) = mem::transmute(self as *const Task);
        TaskRef(task_ref)
    }

    pub unsafe fn into_result(self) -> R {
        self.result.into_inner().unwrap()
    }

}

impl<R> AsTaskRef for TaskAtomicBoxed<R> {
    unsafe fn as_task_ref(&self) -> TaskRef {
        let task_ref: *const (Task + 'static) = mem::transmute(self as *const Task);
        TaskRef(task_ref)
    }
}

impl<R> Task for TaskAtomicBoxed<R> {
    unsafe fn execute(&self) {
        struct PanicGuard<'a>(&'a AtomicBool);
        impl<'a> Drop for PanicGuard<'a> {
            fn drop(&mut self) {
                self.0.store(true,Ordering::Release);
            }
        }

        let _guard = PanicGuard(&self.sync);
        let func: Box<_> = (*self.func.get()).take().unwrap();
        (*self.result.get()) = Some(func())
    }

    unsafe fn done(&self) -> bool {
        self.sync.load(Ordering::Acquire)
    }
}
