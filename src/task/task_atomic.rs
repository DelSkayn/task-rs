use super::{Task,TaskRef,AsTaskRef};

use std::cell::UnsafeCell;
use std::sync::atomic::{Ordering,AtomicBool};
use std::mem;

pub struct TaskAtomic<F,R>
    where F: FnOnce() -> R + Send
{
    sync: AtomicBool,
    func: UnsafeCell<Option<F>>,
    result: UnsafeCell<Option<R>>,
}

impl<F,R> TaskAtomic<F,R>
    where F: FnOnce() -> R + Send
{
    pub unsafe fn new(func: F) -> Self{
        TaskAtomic{
            sync: AtomicBool::new(false),
            func: UnsafeCell::new(Some(func)),
            result:UnsafeCell::new(None),
        }
    }

    pub unsafe fn execute_inline(self) -> R{
        self.func.into_inner().unwrap()()
    }

    pub unsafe fn as_task_ref(&self) -> TaskRef{
        let task_ref: *const (Task + 'static) = mem::transmute(self as *const Task);
        TaskRef(task_ref)
    }

    pub unsafe fn into_result(&self) -> R {
        (*self.result.get()).take().expect("Into result called on a task which was not ready!")
    }

}

impl<F,R> AsTaskRef for TaskAtomic<F,R>
    where F: FnOnce() -> R + Send
{
    unsafe fn as_task_ref(&self) -> TaskRef{
        let task_ref: *const (Task + 'static) = mem::transmute(self as *const Task);
        TaskRef(task_ref)
    }
}

impl<F,R> Task for TaskAtomic<F,R>
where F:FnOnce() -> R + Send
{
    unsafe fn execute(&self){
        struct PanicGuard<'a>(&'a AtomicBool);
        impl<'a> Drop for PanicGuard<'a> {
            fn drop(&mut self) {
                self.0.store(true,Ordering::Release);
            }
        }

        let _guard = PanicGuard(&self.sync);
        let func = (*self.func.get()).take().unwrap();
        (*self.result.get()) = Some(func());
    }

    unsafe fn done(&self) -> bool{
        self.sync.load(Ordering::Acquire)
    }
}
