//!
//! A implementation for jobs. In order to send the tasks across threads we need to wrap it so we
//! can execute the function on a thread. 
//!
//! Implementation based on the rayon library.
//!


mod task_atomic;
//mod task_atomic_boxed;
pub mod local_task;
pub mod task_arena;

pub use self::task_atomic::TaskAtomic;
//pub use self::task_atomic_boxed::TaskAtomicBoxed;
pub use self::local_task::LocalTask;
pub use self::task_arena::TaskArena;

pub trait Task{
    unsafe fn execute(&self);

    unsafe fn done(&self) -> bool;
}

pub trait AsTaskRef{
    unsafe fn as_task_ref(&self) -> TaskRef;
}

impl<'a,T: AsTaskRef + 'a> AsTaskRef for &'a T{
    unsafe fn as_task_ref(&self) -> TaskRef{
        (*self).as_task_ref()
    }
}

#[derive(Copy, Clone,Eq,PartialEq)]
pub struct TaskRef(pub *const Task);
unsafe impl Send for TaskRef{}
unsafe impl Sync for TaskRef{}



