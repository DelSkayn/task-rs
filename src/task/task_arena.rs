use super::local_task::LocalTask;

use std::mem;
use std::ptr;
use std::cell::UnsafeCell;


thread_local!{
    static TASK_ARENA: UnsafeCell<TaskArena> = UnsafeCell::new(TaskArena::new());
}

const START_SIZE: usize = 8;

pub struct TaskArena{
    memory: Vec<Box<[LocalTask]>>,
    free: *mut LocalTask,
    next_size: usize,
}

impl TaskArena{
    fn new() -> Self{
        TaskArena{
            memory: Vec::new(),
            free: ptr::null_mut(),
            next_size: START_SIZE,
        }
    }

    pub unsafe fn alloc() -> *mut LocalTask{
        TASK_ARENA.with(|e|{
            let e = &mut (*e.get());
            if e.free == ptr::null_mut(){
                TaskArena::grow();
            }
            let res = e.free;
            e.free = ptr::read(mem::transmute(res));
            res
        }) 
    }

    pub unsafe fn alloc_new(val: LocalTask) -> *mut LocalTask{
        TASK_ARENA.with(|e|{
            let e = &mut (*e.get());
            if e.free == ptr::null_mut(){
                TaskArena::grow();
            }
            let res = e.free;
            e.free = ptr::read(mem::transmute(res));
            ptr::write(res,val);
            res
        }) 
    }

    unsafe fn grow(){
        TASK_ARENA.with(|e|{
            let e = &mut (*e.get());
            let mut buffer = Vec::with_capacity(e.next_size);
            buffer.set_len(e.next_size);
            for i in 1..e.next_size{
                let ptr: *mut *mut LocalTask = mem::transmute(&mut buffer[i-1]);
                ptr::write(ptr,&mut buffer[i] as *mut LocalTask);
            }
            let ptr: *mut *mut LocalTask = mem::transmute(buffer.last().unwrap());
            ptr::write(ptr,ptr::null_mut());
            e.next_size *= 2;
            e.memory.push(buffer.into_boxed_slice());
            e.free = &mut e.memory.last_mut().unwrap()[0] as *mut LocalTask;
        });
    }

    pub unsafe fn dealloc(val: *mut LocalTask){
        TASK_ARENA.with(|e|{
            let ptr: *mut *mut LocalTask = mem::transmute(val);
            ptr::write(ptr,(*e.get()).free);
            (*e.get()).free = val;
        });
    }
}

impl Drop for TaskArena{
    fn drop(&mut self){
        unsafe{
            self.memory.set_len(0);
        }
    }
}
