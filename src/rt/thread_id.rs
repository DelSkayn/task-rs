

use std::sync::Mutex;
use std::cell::Cell;
use std::sync::atomic::{AtomicUsize,Ordering};
use std::collections::HashMap;


implement_hotloading!{
    static THREAD_ID_DISTRIBUTER: ThreadIdDistributer = ThreadIdDistributer::new();
}


thread_local!{ 
    pub static THREAD_ID: Cell<usize> = Cell::new(THREAD_ID_DISTRIBUTER.get_thread_id());
}

pub struct ThreadIdDistributer(Mutex<HashMap<usize,usize>>, AtomicUsize);

/// A struct representing the id of a thread. 
/// Can be used to compare and address threads.
#[derive(Copy,Clone,Hash,Eq,PartialEq,Debug)]
pub struct ThreadId(usize);


impl ThreadId{
    /// Returns the id of the current thread.
    pub fn current() -> Self{
        ThreadId(THREAD_ID.with(|e| e.get()))
    }

    /// Returns the id of the main thread form which the program was
    /// started.
    pub fn main() -> Self{
        ThreadId(0)
    }

    /// Returns the num'th thread.
    /// If the num is large then the amount of threads running
    /// if will be the thread id of the last thread.
    pub fn as_usize(self) -> usize{
        self.0
    }
}

impl From<usize> for ThreadId{
    fn from(u: usize) -> Self{
        ThreadId(u)
    }
}
impl ThreadIdDistributer{
    fn new() -> Self{
        ThreadIdDistributer(Mutex::new(HashMap::new()),AtomicUsize::new(0))
    }

    /// Gets thread id even if loaded from runtime.
    fn get_thread_id(&self) -> usize{
        let sys_id = sys::get_system_thread_id();
        trace!("[Task] thread id requested for thread with id: \"{}\".",sys_id);
        let mut borrow = self.0.lock().unwrap();
        if let Some(x) = borrow.get(&sys_id){
            trace!("[Task] Id already created for thread passing old one.!");
            return *x;
        }
        trace!("[Task] new thread id created!");
        let new_id = self.1.fetch_add(1,Ordering::Acquire);
        borrow.insert(sys_id,new_id);
        new_id
    }

}

mod sys{
//! Module specifying some system specific functions

#[cfg(unix)]
extern crate libc;

#[cfg(windows)]
extern crate kernel32;

#[cfg(unix)]
#[inline]
pub fn get_system_thread_id() -> usize{
    unsafe { libc::pthread_self() as usize }
}

#[cfg(windows)]
#[inline]
pub fn get_system_thread_id() -> usize{
    unsafe { kernel32::GetCurrentThreadId() as usize }
}
}
