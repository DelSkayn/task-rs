pub mod que;
pub mod worker;
pub mod runtime;
pub mod thread_id;
pub mod sleep;

extern crate crossbeam;

pub use self::que::{GlobalQue,StealQue};
pub use self::runtime::{Runtime,JoinHandle};

pub mod hotload{
    use super::runtime::{self, Runtime};
    use super::thread_id::{self,ThreadIdDistributer};
    use super::sleep::{self, Sleep};

    pub type LoadFunc = extern fn(rt: RT);

    #[derive(Debug)]
    #[repr(C)]
    pub struct RT{
        r: *const Runtime,
        tid: *const ThreadIdDistributer,
        s: *const Sleep,
    }

    #[no_mangle]
    pub unsafe extern fn __task_load_runtime(rt: RT){
        assert!(!rt.r.is_null());
        assert!(!rt.tid.is_null());
        assert!(!rt.s.is_null());
        runtime::hotload::receive(rt.r);
        thread_id::hotload::receive(rt.tid);
        sleep::hotload::receive(rt.s);
    }

    pub fn get_runtime() -> RT{
        unsafe{ 
            RT{
                r: runtime::hotload::provide(),
                tid: thread_id::hotload::provide(),
                s: sleep::hotload::provide(),
            }
        }
    }
}



