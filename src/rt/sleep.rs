use std::thread::Thread;
use std::thread;
use std::ptr;

use std::sync::atomic::{Ordering,AtomicPtr,AtomicBool};

use ::ThreadId;

//use std::time::Duration;


implement_hotloading!{
    static SLEEP: Sleep = Sleep::new();
}

pub struct Sleep(AtomicPtr<Node>);

struct Node{
    next: AtomicPtr<Node>,
    thread: Thread,
    unpark: AtomicBool,
}

/// Maybe implement a way to wake specific threads
impl Sleep {
    pub fn new() -> Self {
        Sleep(AtomicPtr::new(ptr::null_mut()))
    }

    /// Wakes all threads waiting on this sleep
    /// Returns wether someone waked up
    pub fn wake(&self){
        let mut ptr = self.0.swap(ptr::null_mut(),Ordering::AcqRel);
        while ptr != ptr::null_mut(){
            unsafe{
                let node = &(*ptr);
                node.unpark.store(true,Ordering::Release);
                ptr = node.next.load(Ordering::Acquire);
                node.thread.unpark();
            }
        }
    }



    /// Puts the current thread to sleep untill an other thread wakes it up.
    /// In the library threads are mostly awoken after a new task is made avaible for processing.
    pub fn sleep(&self) {
        trace!("[Task] Thread \"{}\" going to sleep",ThreadId::current().as_usize());
        let mut node = Node{
            next: AtomicPtr::new(ptr::null_mut()),
            thread: thread::current(),
            unpark: AtomicBool::new(false),
        };
        loop{
            let next = self.0.load(Ordering::Acquire);
            node.next.store(next,Ordering::Release);
            if self.0.compare_and_swap
                ( next
                , &mut node as *mut _
                ,Ordering::AcqRel) 
                    == next {
                loop{
                    thread::park();
                    // thread can wake up early 
                    // prevent waking from sleep 
                    // by parking if no wake has taken place.
                    if node.unpark.load(Ordering::Acquire){
                        trace!("[Task] Thread \"{}\" Woke up",ThreadId::current().as_usize());
                        return
                    }
                }
            }
        }
    }

    pub fn global_sleep(){
        SLEEP.sleep();
    }

    pub fn global_wake(){
        SLEEP.wake();
    }
}
