use super::{GlobalQue, StealQue};
use super::thread_id::THREAD_ID;
use super::runtime::{Steal, Runtime};

use ::rt::sleep::Sleep;

pub struct WorkerThread;

impl WorkerThread{
    pub fn run() {
        let id = THREAD_ID.with(|e| e.get());
        trace!("[Task] Worker thread \"{}\" started",id);
        loop {
            if work() {
                continue;
            }
            Runtime::hit_sync();
            Sleep::global_sleep();
        }
    }
}

pub fn work() -> bool {
    if let Steal::Data(x) = StealQue::steal() {
        unsafe { (*x.0).execute() }
        true
    } else if let Some(x) = GlobalQue::pop() {
        unsafe { (*x.0).execute() }
        true
    } else {
        false
    }
}
