use ::task::TaskRef;
use ::config::CONFIG;
use super::worker::WorkerThread;
use super::thread_id::ThreadId;

use super::crossbeam::sync::SegQueue;
pub use super::crossbeam::sync::chase_lev::Steal;
use super::crossbeam::sync::chase_lev::{
    self,
    Worker,
    Stealer,
};
use super::sleep::Sleep;

use std::sync::atomic::{Ordering,AtomicUsize};
use std::cell::UnsafeCell;
use std::thread::{self,JoinHandle as StdJoinHandle};


implement_hotloading!{
    static RUNTIME: Runtime = Runtime::new();
}

pub struct JoinHandle(StdJoinHandle<()>);

impl JoinHandle{
    pub fn join(self){
        self.0.join().ok();
    }
}

pub struct Runtime{
    /// the workers for work stealing indexed by threadid,
    pub workers: UnsafeCell<Vec<Worker<TaskRef>>>,
    /// the stealers for work stealing indexed by threadid,
    pub stealers: UnsafeCell<Vec<Stealer<TaskRef>>>,
    /// the global que,
    pub global: SegQueue<TaskRef>,
    /// Whether the threads should syncronize
    sync: AtomicUsize,
    /// The count of the threads are already syncronized.
    synced_threads: AtomicUsize,
    /// The sleep used for waiting till syncronization is over.
    sync_sleep: Sleep,
    /// Amount of threads THIS INCLUDES THE MAIN THREAD!!!!
    amount: AtomicUsize,
}

impl Runtime{
    fn new() -> Self{
        trace!("[Task] Starting runtime!");
        let amount = CONFIG.read().unwrap().thread_amount;
        // Making sure main thread has thread id 0;
        ThreadId::current();

        for _ in 0..amount{
            thread::spawn(|| WorkerThread::run());
        }

        let (workers,stealers) = (0..amount+1).map(|_| chase_lev::deque()).unzip();

        Runtime{
            workers: UnsafeCell::new(workers),
            stealers: UnsafeCell::new(stealers),
            global: SegQueue::new(),
            sync: AtomicUsize::new(0),
            synced_threads: AtomicUsize::new(0),
            sync_sleep: Sleep::new(),
            amount: AtomicUsize::new(amount + 1),
        }
    }

    /// Returns the amount of threads
    pub fn amount_of_threads() -> usize{
        RUNTIME.amount.load(Ordering::Acquire)
    }

    /// Create a new thread.
    /// Can only be safely executed during a syncronization
    pub unsafe fn new_thread<F: FnOnce() + Send + 'static>(func: F) -> JoinHandle{
        let new_id = RUNTIME.amount.fetch_add(1,Ordering::Acquire);
        let (w,s) = chase_lev::deque::<TaskRef>();

        assert!((*RUNTIME.stealers.get()).len() == new_id);
        (*RUNTIME.stealers.get()).push(s);
        assert!((*RUNTIME.workers.get()).len() == new_id);
        (*RUNTIME.workers.get()).push(w);

        JoinHandle(thread::spawn(move ||{
            ThreadId::current();
            trace!("Started new managed thread with the id \"{}\"",new_id);
            Runtime::hit_sync();
            func();
        }))
    }

    /// Starts the runtime.
    pub fn start(){
        &RUNTIME.amount;
    }

    /// Syncronize if nessecary
    pub fn hit_sync() -> bool{
        if RUNTIME.sync.load(Ordering::Acquire) != 0 {
            RUNTIME.synced_threads.fetch_add(1,Ordering::Release);
            trace!("[Task] Thread {:?} hit sync_point.",ThreadId::current());
            // This must stay a while loop because other threads might wake op a
            // sycnronized thread if there still doing work.
            RUNTIME.sync_sleep.sleep();
            trace!("[Task] Thread {:?} woken up from sync_point",ThreadId::current());
            true
        }else{
            false
        }
    }

    pub fn is_syncronizing() -> bool{
        RUNTIME.sync.load(Ordering::Acquire) != 0
    }

    /// Start syncronization
    pub fn syncronize(){
        trace!("[Task] Syncronizing!");
        if ThreadId::current() != ThreadId::main() {
            panic!("Runtime can only syncronize from main thread!");
        }
        // Add one to sync allowing multiple calls to syncronize to be nested.
        if RUNTIME.sync.fetch_add(1,Ordering::AcqRel) == 0{
            // Wait till all threads are finished
            let len = RUNTIME.amount.load(Ordering::Acquire);
            while RUNTIME.synced_threads.load(Ordering::Acquire) != len - 1 {
                Sleep::global_wake();
                thread::yield_now();
            }
        }
    }

    /// Finish syncronization
    pub fn desyncronize(){
        trace!("[Task] Desyncronizing!");
        if RUNTIME.sync.fetch_sub(1,Ordering::AcqRel) == 1{
            // reset syncthreads
            RUNTIME.synced_threads.store(0,Ordering::Release);
            RUNTIME.sync_sleep.wake();
            trace!("[Task] Desyncronized!");
        }
    }
}
