extern crate rand;

use super::thread_id::{THREAD_ID};
use super::runtime::{RUNTIME, Runtime, Steal};
use task::TaskRef;

use self::rand::{Rng,SeedableRng,XorShiftRng};

use std::cell::UnsafeCell;

thread_local!{
    static RANDOM: UnsafeCell<XorShiftRng> = {
        let id = THREAD_ID.with(|e| e.get());
        UnsafeCell::new(XorShiftRng::from_seed([0,0,0,(id+1) as u32]))
    };
}

pub struct GlobalQue;

impl GlobalQue{
    #[inline]
    pub fn pop() -> Option<TaskRef>{
        RUNTIME.global.try_pop()
    }

    #[inline]
    pub fn push(t: TaskRef){
        RUNTIME.global.push(t); 
    }
}


pub struct StealQue;

impl StealQue{

    #[inline]
    pub fn push(t: TaskRef){
        let id = THREAD_ID.with(|e| e.get());
        unsafe{ (*RUNTIME.workers.get())[id].push(t) };
    }

    #[inline]
    pub fn pop() -> Option<TaskRef>{
        let id = THREAD_ID.with(|e| e.get());
        unsafe{ (*RUNTIME.workers.get())[id].try_pop() }
    }

    pub fn steal() -> Steal<TaskRef>{
        unsafe{
            let id = THREAD_ID.with(|e| e.get());
            let amount = Runtime::amount_of_threads();
            let rand: usize = RANDOM.with(|e| { (*e.get()).gen::<usize>() % amount });
            let mut more = false;

            for i in rand..amount{
                if i == id{
                    continue;
                }

                match (*RUNTIME.stealers.get())[i].steal(){
                    Steal::Empty => {},
                    Steal::Abort => {more = true},
                    Steal::Data(x) => return Steal::Data(x),
                }
            }

            for i in 0..rand{
                if i == id{
                    continue;
                }

                match (*RUNTIME.stealers.get())[i].steal(){
                    Steal::Empty => {},
                    Steal::Abort => {more = true},
                    Steal::Data(x) => return Steal::Data(x),
                }
            }
            if more{
                Steal::Abort
            }else{
                Steal::Empty
            }
        }
    }
}

