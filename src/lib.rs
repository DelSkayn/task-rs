//!
//! Task is a library build for paralizing applications.
//!
#![allow(dead_code)]
#![feature(const_fn)]
#![feature(conservative_impl_trait)]

#[macro_use]
extern crate lazy_static;
#[macro_use]
extern crate log;
extern crate crossbeam;

#[macro_use]
mod macros;
mod rt;
mod task;
pub mod config;
#[cfg(test)]
mod test;
mod api;

pub mod sync;
pub use rt::hotload;

pub use ::rt::thread_id::{ThreadId};
pub use ::api::*;
pub use ::config::Config;
