extern crate num_cpus;

use std::sync::RwLock;

lazy_static!{
    pub static ref CONFIG: RwLock<Config> = RwLock::new(Default::default());
}

/// A struct used to configure the runtime of task rs
pub struct Config {

    /// The amount of worker threads to be created.
    /// By default one less than the amount of cores on the machine.
    pub thread_amount: usize,
}

impl Config {
    pub fn new() -> Self {
        Config { thread_amount: 0 }
    }
}

impl Default for Config {
    fn default() -> Config {
        Config { thread_amount: num_cpus::get() - 1}
    }
}

/// Set the configuration of the runtime.
/// # Panic
/// - The runtime will panic if the amount of thread is set to 0
pub fn configure(conf: Config) {
    *CONFIG.write().unwrap() = conf;
}
