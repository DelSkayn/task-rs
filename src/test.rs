
use super::*;

fn fibbo_par(i: u64) -> u64 {
    match i {
        0 => 1,
        1 => 1,
        _ => {
            let (a, b) = join(|| fibbo(i - 1), || fibbo(i - 2));
            a + b
        }
    }
}

fn fibbo(i: u64) -> u64 {
    match i {
        0 => 1,
        1 => 1,
        _ => fibbo(i - 1) + fibbo(i - 2),
    }
}

#[test]
#[should_panic]
fn panic() {
    let mut config = Config::new();
    config.thread_amount = 3;
    config::configure(config);
    join(|| {
             // do some work;
             // so that the job gets stolen
             fibbo(30);
         },
         || {
             panic!("From job");
         });
}

#[test]
fn correct_calc() {
    let mut config = Config::new();
    config.thread_amount = 3;
    config::configure(config);
    assert_eq!(fibbo_par(30), fibbo(30));
}
