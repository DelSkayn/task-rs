
macro_rules! implement_hotloading{
    (static $name:ident : $T:ty = $e:expr;) => {
        pub use self::__hotload::{$name,hotload};

        mod __hotload{
            use super::*;
            use std::ops::Deref;
            use std::cell::UnsafeCell;
            use std::sync::{Once,ONCE_INIT};
            use std::ptr;

            pub struct __WRAP {
                _inner: UnsafeCell<*const $T>,
            }

            pub static $name : __WRAP = __WRAP{
                _inner: UnsafeCell::new(ptr::null()),
            };
            static __START : Once = ONCE_INIT;

            unsafe impl Sync for __WRAP{}

            impl Deref for __WRAP{
                type Target = $T;

                fn deref(&self) -> &$T{
                    unsafe{
                        __START.call_once(||{
                            (*self._inner.get()) = Box::into_raw(Box::new($e));
                        });
                        &*(*self._inner.get())
                    }
                }
            }

            pub mod hotload{
                use super::*;

                pub unsafe fn receive(v: *const $T){
                    let mut executed = false;
                    __START.call_once(||{
                        (*$name._inner.get()) = v;
                        executed = true;
                    });
                    if !executed {
                        panic!("A pointer for hotloading was provided to a already initialized value.");
                    }
                }

                pub unsafe fn provide() -> *const $T{
                    __START.call_once(||{
                        (*$name._inner.get()) = Box::into_raw(Box::new($e));
                    });
                    *$name._inner.get()
                }
            }
        }
    }
}
