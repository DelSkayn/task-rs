Task-rs
=======

An safe easy and fast parallel processing library for the rust programming language. This library takes heavy insperation of [Rayon](https://github.com/nikomatsakis/rayon).

The focus of this library is to provide a framework for multithreading, specificly for the axle game engine. It can probebly be used for other projects and has no dependency on axle.
But it is not focused on providing a stable library while axle is not yet stable. 

